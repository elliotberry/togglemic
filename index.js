const { exec } = require("child_process");
const notifier = require("node-notifier");

function execShellCommand(cmd) {
  return new Promise((res, rej) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        rej(error);
      } else if (stderr) {
        rej(stderr);
      } else {
        res(stdout);
      }
    });
  });
}

async function getVol() {
  let currentVol = await execShellCommand(`osascript -e "get volume settings"`);

  return parseInt(currentVol.split(" ")[3].replace(/\D/g, ""));
}

async function setVol(perc) {
  await execShellCommand(
    `osascript -e 'tell application "System Events" to set volume input volume ${perc}'`
  );
}

async function toggle() {
  let inputVol = await getVol();
  if (inputVol > 0) {
    await setVol("0");
    notifier.notify("Mic volume 0");
  } else {
    await setVol("100");
    notifier.notify("Mic volume 100");
  }
}
toggle();
